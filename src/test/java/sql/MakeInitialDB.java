package sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import sql.Connector;

public class MakeInitialDB {
  public static void main(String[] args) {
    Connection con = Connector.getConnection();
    try {
      con.createStatement().executeUpdate("drop table foods");
      con.createStatement().executeUpdate("drop table dependencies");
      con.createStatement().executeUpdate("drop table details");
      con.createStatement().executeUpdate("create table if not exists foods (foodid text unique, name text, price int unique, type text)");
      con.createStatement().executeUpdate("create table if not exists dependencies (foodid text, dependencyType text, dependencyId text)");
      con.createStatement().executeUpdate("create table if not exists details (foodid text, detailName text, option text, detailPrice int)");

      String types[] = {"Tonkotu", "Shoyu", "Saltfravored"};

      for (int i = 0; i < 3; i++) {
        String type = String.copyValueOf(types[i].toCharArray(), 0, 3);

        Connector.insertFood(
              con,
              "Dx " + types[i] + " Ramen",
              1000 - i * 10,
              type
        );

        Connector.insertFood(
              con,
              "Ajitama " + types[i] + " Ramen",
              800 - i * 10,
            type
        );

        Connector.insertFood(
              con,
              "Chashu " + types[i] + " Ramen",
              900 - i * 10,
            type
        );

        Connector.insertFood(
              con,
              types[i] + " Ramen",
              700 - i * 10,
            type
        );
      }

      Connector.insertFood(
            con,
            "Rice",
            200,
            "Ric"
      );

      Connector.insertFood(
              con,
              "Chashu Don",
              350,
              "Ric"
      );

      Connector.insertFood(
              con,
              "Charhan",
              300,
              "Ric"
      );

      Connector.insertFood(
              con,
              "Gyoza",
              400,
              "Sid"
      );

      Connector.insertFood(
              con,
              "Karaage",
              450,
              "Sid"
      );

      Connector.insertFood(
          con,
          "Edamame",
          210,
          "Sid"
      );

      Connector.insertFood(
          con,
          "Rice set",
          100,
          "Set"
      );

      Connector.insertFood(
          con,
          "Charhan set",
          150,
          "Set"
      );

        Connector.insertFood(
            con,
            "Gyoza set",
            215,
            "Set"
        );

        Connector.insertFood(
            con,
            "Karaage set",
            250,
            "Set"
        );

        Connector.insertFood(
            con,
            "Short Cake",
            405,
            "Des"
        );

        Connector.insertFood(
            con,
            "Cheese Cake",
            410,
            "Des"
        );

        Connector.insertFood(
            con,
            "Tiramisu",
            420,
            "Des"
        );

        Connector.insertFood(
            con,
            "Ice Cream",
            190,
            "Des"
        );

        Connector.insertFood(
            con,
            "Coffee",
            510,
            "Dri"
        );

        Connector.insertFood(
            con,
            "Tea",
            490,
            "Dri"
        );

        Connector.insertFood(
            con,
            "Orange Juice",
            480,
            "Dri"
        );

        Connector.insertFood(
            con,
            "Cola",
            470,
            "Dri"
        );

        Connector.insertFood(
            con,
            "Beer",
            695,
            "Dri"
        );

        Connector.insertFood(
            con,
            "Wine",
            810,
            "Dri"
        );

        Connector.insertFood(
            con,
            "Chashu",
            108,
            "Top"
        );

        Connector.insertFood(
            con,
            "Ajitama",
            50,
            "Top"
        );

        Connector.insertFood(
            con,
            "Nori",
            30,
            "Top"
        );

      Statement stmt = con.createStatement();
        stmt.setQueryTimeout(30);
        ResultSet rs = stmt.executeQuery("select * from foods where type = 'Ton' or type = 'Sho' or type = 'Sal'");

        while (rs.next()) {
          String foodid = rs.getString("foodid");
          String type = rs.getString("type");
          Connector.insertDetail(
              con,
              foodid,
              "Noodle",
              "Soft",
              0
          );
          Connector.insertDetail(
              con,
              foodid,
              "Noodle",
              "Hard",
              0
          );
          Connector.insertDetail(
              con,
              foodid,
              "Noodle",
              "Extra Hard",
              0
          );

              Connector.insertDetail(
              con,
              foodid,
              "Amount",
              "Large",
              100
          );
            Connector.insertDetail(
                con,
                foodid,
                "Amount",
                "Small",
                -100
            );
        }

        rs = stmt.executeQuery("select * from foods where type = 'Set'");
        while (rs.next()) {
            String foodid = rs.getString("foodid");

            Connector.insetDependency(
                con,
                foodid,
                "Ton"
            );
            Connector.insetDependency(
                con,
                foodid,
                "Sho"
            );
            Connector.insetDependency(
                con,
                foodid,
                "Sal"
            );
        }

    } catch (Exception e) {
      e.printStackTrace();
    }

  }
}
