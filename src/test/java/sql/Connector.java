package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.UUID;

/**.
 * データ保管用DBとの接続確立. <br>
 * getConnection : コネクションを確率して返す
 */
public class Connector {
    private static Connection connection = null;

    /**.
     * コネクションを生成して返す
     * @return Connection
     */
    public static Connection getConnection() {
        try {
            //コネクション生成
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:"+ getResourcesPath("foods.db"));

            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);

            statement.executeUpdate("create table if not exists foods (foodid text unique, name text, price int unique, imagepath text, type text)");
            statement.executeUpdate("create table if not exists dependencies (foodid text, dependencyType text, dependencyId text)");
            statement.executeUpdate("create table if not exists details (foodid text, detailName text, option text, detailPrice int)");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void insertFood(Connection con, String name, int price, String type) {
        try {
            PreparedStatement ps = con.prepareStatement("INSERT INTO foods (foodid, name, price, type) VALUES (?, ?, ?, ?)");

            ps.setString(1, UUID.randomUUID().toString());
            ps.setString(2, name);
            ps.setInt(3, price);
            ps.setString(4, type);

            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(price);
            e.printStackTrace();
        }
    }

    public static void insertDetail(Connection con, String foodid, String detailName, String option, int detailPrice) {
        try {
            PreparedStatement ps = con.prepareStatement("INSERT INTO details (foodid, detailName, option, detailPrice) VALUES (?, ?, ?, ?)");

            ps.setString(1, foodid);
            ps.setString(2, detailName);
            ps.setString(3, option);
            ps.setInt(4, detailPrice);

            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(detailPrice);
            e.printStackTrace();
        }
    }

    public static void insetDependency(Connection con, String foodid, String dependencyId) {
        try {
            PreparedStatement ps = con.prepareStatement("INSERT INTO dependencies (foodid, dependencyType) VALUES (?, ?)");

            ps.setString(1, foodid);
            ps.setString(2, dependencyId);

            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getResourcesPath(String s) {
        return "src\\test\\resources\\" + s;
    }
}
