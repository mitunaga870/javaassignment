import foods.Food;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;
import order.OrderFood;

public class OrderTableModel extends AbstractTableModel {
    Main main;

    public OrderTableModel(Main main) {
        super();
        this.main = main;
    }

    @Override
    public int getRowCount() {
        return main.orderFoodList.getLenOfItem();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        OrderFood food = main.orderFoodList.get(rowIndex);
        switch (columnIndex) {
            case 0 -> {
                ImageIcon icon = new ImageIcon(food.getImagePath());
                icon = new ImageIcon(icon.getImage().getScaledInstance(200, 200, java.awt.Image.SCALE_SMOOTH));
                return icon;
            }
            case 1 -> {
                return food.getName();
            }
            case 2 -> {
                return food.getDetailText();
            }
            case 3 -> {
                return food.amount;
            }
            case 4 -> {
                return food.getPrice();
            }
            default -> {
                return null;
            }
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0 -> {
                return ImageIcon.class;
            }
            case 1, 2 -> {
                return String.class;
            }
            case 3, 4 -> {
                return Integer.class;
            }
            default -> {
                return null;
            }
        }
    }
}
