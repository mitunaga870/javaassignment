package sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**.
 * データ保管用DBとの接続確立. <br>
 * getConnection : コネクションを確率して返す
 */
public class Connector {
    private static Connection connection = null;

    /**.
     * コネクションを生成して返す
     * @return Connection
     */
    public static Connection getConnection() {
        try {
            //コネクション生成
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:"+ getResourcesPath("foods.db"));

            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);

            statement.executeUpdate("create table if not exists system (password text unique)");
            statement.executeUpdate("create table if not exists foods (foodid text unique, name text, price int unique, type text)");
            statement.executeUpdate("create table if not exists dependencies (foodid text, dependencyType text, dependencyId text)");
            statement.executeUpdate("create table if not exists details (foodid text, detailName text, option text, detailPrice int)");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    private static String getResourcesPath(String s) {
        return "src\\main\\resources\\" + s;
    }
}
