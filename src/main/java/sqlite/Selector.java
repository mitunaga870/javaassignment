package sqlite;

import enums.DependencyTypeClass.DependencyType;
import enums.FoodGenreTypeClass;
import enums.FoodGenreTypeClass.FoodGenreType;
import foods.Detail;
import foods.Food;
import foods.FoodList;
import foods.Menu;
import foods.MenuSet;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Objects;

/**.
 * DBから情報を取得するクラス
 * GetMenu: メニューセットを取得する
 */
public class Selector {
    /**.
     * メニューセットを取得する
     * @return メニューセット
     */
    public static MenuSet getMenu() {
        try {
            // DBとの接続を確立
            Connection con = Connector.getConnection();

            // メニューセットを生成
            MenuSet menuSet = new MenuSet(
                    new Menu(FoodGenreType.Dessert),
                    new Menu(FoodGenreType.Drink),
                    new Menu(FoodGenreType.Rice),
                    new Menu(FoodGenreType.Saltflavoured),
                    new Menu(FoodGenreType.Set),
                    new Menu(FoodGenreType.Shoyu),
                    new Menu(FoodGenreType.Side),
                    new Menu(FoodGenreType.Tonkotu),
                    new Menu(FoodGenreType.Topping)
            );

            // SQL文を実行
            Statement stmt = con.createStatement();
            stmt.setQueryTimeout(30);
            ResultSet rs = stmt.executeQuery("select * from foods left outer join details on foods.foodid = details.foodid left outer join dependencies on foods.foodid = dependencies.foodid");
            while (rs.next()) {
                //get Food data
                FoodGenreType type = FoodGenreTypeClass.getFoodGenreType(rs.getString("type"));
                String foodid = rs.getString("foodid");
                String name = rs.getString("name");
                int price = rs.getInt("price");
                //get Detail data
                String detailName = rs.getString("detailName");
                String option = rs.getString("option");
                int detailPrice = rs.getInt("detailPrice");
                //get Dependency data
                String dependencyType = rs.getString("dependencyType");
                String dependencyId = rs.getString("dependencyId");
                //両方あるのは不正なデータなので無視
                if (dependencyType != null && dependencyId != null) {
                    continue;
                }
                //get Menu
                Menu menu = menuSet.getMenu(Objects.requireNonNull(type));

                //Get Food
                Food food = new Food(foodid, name, price, type);
                //Add to Menu
                if (menu.contains(foodid)) {
                    food = menu.get(foodid);
                } else {
                    menu.add(food);
                }

                //Set Detail
                if (detailName != null) {
                    food.addDetail(detailName, option, detailPrice);
                }

                //Set Dependency
                if (dependencyType != null) {
                    food.addDependency(DependencyType.FoodGenreType,dependencyType);
                } else if (dependencyId != null) {
                    food.addDependency(DependencyType.FoodId,dependencyId);
                }
            }

            // DBとの接続を切断
            con.close();

            return menuSet;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**.
     * パスワードを取得する
     * @return password
     */
    public static String getPassword() {
        try {
            Connection con = Connector.getConnection();

            ResultSet rs = con.createStatement().executeQuery("select * from system");
            if (rs.next()) {
                String password = rs.getString("password");
                con.close();
                return password;
            } else {
                con.close();
                return "password";
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static ArrayList<Food> getFoods() {
        try {
            Connection con = Connector.getConnection();

            ArrayList<Food> foods = new ArrayList<>();

            ResultSet rs = con.createStatement().executeQuery("select * from foods");
            while (rs.next()) {
                String foodid = rs.getString("foodid");
                String name = rs.getString("name");
                int price = rs.getInt("price");
                FoodGenreType type = FoodGenreTypeClass.getFoodGenreType(rs.getString("type"));
                foods.add(new Food(foodid, name, price, type));
            }

            con.close();

            return foods;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
