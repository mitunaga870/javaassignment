package sqlite;

import enums.DependencyTypeClass;
import foods.Dependency;
import foods.Detail;
import foods.Food;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Map;

/**.
 * DBに情報を挿入するクラス
 */
public class Inserter {

    /**
     * .
     * 新規メニューを追加・更新する
     *
     * @param food 追加・更新するメニュー
     * @return boolean 成功したかどうか
     */
    public static boolean insertFood(Food food) {
        try {
            Connection con = Connector.getConnection();
            // delete to foods
            String sql = "DELETE FROM foods WHERE foodid = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, food.getFoodId());
            stmt.executeUpdate();
            stmt.close();

            // delete to dependencies
            sql = "DELETE FROM dependencies WHERE foodid = ?";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, food.getFoodId());
            stmt.executeUpdate();
            stmt.close();
            // delete to details
            sql = "DELETE FROM details WHERE foodid = ?";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, food.getFoodId());
            stmt.executeUpdate();
            stmt.close();

            // insert to foods
            sql = "INSERT INTO foods (foodid, name, price, imagepath, type) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, food.getFoodId());
            ps.setString(2, food.getName());
            ps.setInt(3, food.getPrice());
            ps.setString(5, food.getType().getString());
            ps.executeUpdate();
            ps.close();
            // insert to dependencies
            for (Dependency dependency : food.getDependencies()) {

                switch (dependency.getDependencyType()) {
                    case DependencyTypeClass.DependencyType.FoodGenreType:
                        sql = "INSERT INTO dependencies (foodid, dependencyType) VALUES (?, ?)";
                        break;
                    case DependencyTypeClass.DependencyType.FoodId:
                        sql = "INSERT INTO dependencies (foodid, dependencyId) VALUES (?, ?)";
                        break;
                    default:
                        throw new Exception("DependencyTypeが不正です");
                }
                ps = con.prepareStatement(sql);
                ps.setString(1, food.getFoodId());
                ps.setString(2, dependency.get());
                ps.executeUpdate();
                ps.close();
            }
            // insert to details
            sql = "INSERT INTO details (foodid, detailName, option, detailPrice) VALUES (?, ?, ?, ?)";
            for (Detail detail : food.getDetails().get()) {
                for (Map.Entry<String, Integer> entry : detail.getOptions().entrySet()) {
                    ps = con.prepareStatement(sql);
                    ps.setString(1, food.getFoodId());
                    ps.setString(2, detail.getName());
                    ps.setString(3, entry.getKey());
                    ps.setInt(4, entry.getValue());
                    ps.executeUpdate();
                    ps.close();
                }
            }
            con.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean insertPassword(String password1) {
        try {
            Connection con = Connector.getConnection();
            String sql = "DELETE FROM system";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.executeUpdate();
            stmt.close();
            sql = "INSERT INTO system (password) VALUES (?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, password1);
            ps.executeUpdate();
            ps.close();
            con.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
