import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

public class PassfordForm {
    private JButton confirm;
    private JButton cancel;
    private JPasswordField passwordField1;
    private JPanel root;
    private JDialog dialog;
    private String password;

    public PassfordForm(JFrame frame) {
        dialog = new JDialog(frame, "Password", true);

        confirm.addActionListener(e -> {
            password = new String(passwordField1.getPassword());
            dialog.dispose();
        });
        cancel.addActionListener(e -> {
            password = null;
            dialog.dispose();
        });
    }

    public String getPassword() {
        dialog.setContentPane(root);
        dialog.setSize(300, 300);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        return password;
    }
}
