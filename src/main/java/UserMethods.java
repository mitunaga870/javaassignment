import javax.swing.JOptionPane;

/**.
 * ユーザー定義のメソッドをまとめたクラス
 * getResoucePath(String path) : リソースのパスを取得する
 * confirmDialog(String title, String message) : 確認ダイアログを表示する
 */
public class UserMethods {
    public static String getResoucePath(String path) {
        return "src/main/resources/" + path;
    }

    public static boolean confirmDialog(String title, String message) {
        return JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
    }

    public static void showError(String title, String message) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public static void showInfo(String success, String successfullySaved) {
        JOptionPane.showMessageDialog(null, successfullySaved, success, JOptionPane.INFORMATION_MESSAGE);
    }
}
