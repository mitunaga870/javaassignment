package ui;

import java.awt.Font;
import javax.swing.JComponent;

public class GeneralFont {
    public static Font mainFont(JComponent c) {
        return new Font("Roboto", Font.PLAIN, 24);
    }
}
