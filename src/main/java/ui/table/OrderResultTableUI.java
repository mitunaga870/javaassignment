package ui.table;

import mdlaf.components.table.MaterialTableUI;
import ui.GeneralFont;
import ui.Reactive;

public class OrderResultTableUI extends MaterialTableUI {
    Reactive reactive;
    public OrderResultTableUI(int maxWidth, int maxHeight) {
        super();
        reactive = new Reactive(maxWidth, maxHeight);
    }

    @Override
    public void installUI(javax.swing.JComponent c) {
        super.installUI(c);
    }
}
