package ui.panel;

import java.awt.Dimension;
import javax.swing.plaf.PanelUI;
import ui.Reactive;

public class GeneralPalelUI extends PanelUI {

    private Reactive reactive;
    private int width;
    private int height;

    public GeneralPalelUI(int maxW, int maxH) {
        super();
        this.reactive = new Reactive(maxW, maxH);
        this.width = 0;
        this.height = 0;
    }

    @Override
    public void installUI(javax.swing.JComponent c) {
        super.installUI(c);
        if (width != 0 && height != 0) {
            c.setPreferredSize(new Dimension(reactive.widhtPer(width), reactive.heightPer(height)));
        }
    }

    public GeneralPalelUI setSize(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }
}
