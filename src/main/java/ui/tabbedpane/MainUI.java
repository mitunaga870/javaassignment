package ui.tabbedpane;

import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.DimensionUIResource;
import javax.swing.plaf.TabbedPaneUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import mdlaf.components.tabbedpane.MaterialTabbedPaneUI;
import ui.Reactive;

public class MainUI extends BasicTabbedPaneUI {
    Reactive reactive;
    int width;
    int height;
    public MainUI(int maxW, int maxH) {
        super();
        this.reactive = new Reactive(maxW, maxH);
        this.width = 0;
        this.height = 0;
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        if (width != 0 && height != 0) {
            c.setPreferredSize(new DimensionUIResource(reactive.widhtPer(width), reactive.heightPer(height)));
        }
    }

    public MainUI setSize(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }
}
