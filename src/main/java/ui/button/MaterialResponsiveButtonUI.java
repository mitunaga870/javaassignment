package ui.button;

import mdlaf.animation.MaterialUIMovement;
import mdlaf.components.button.MaterialButtonUI;
import ui.GeneralFont;
import ui.Reactive;

import javax.swing.*;
import java.awt.*;

public abstract class MaterialResponsiveButtonUI extends MaterialButtonUI {
    protected Reactive reactive;
    public MaterialResponsiveButtonUI(int maxW, int maxH) {
        this.reactive = new Reactive(maxW, maxH);
    }

    public MaterialResponsiveButtonUI(MaterialResponsiveButtonUI base) {
        this.reactive = base.reactive;
    }

    protected void setUI(JComponent c, Color mainColor, int width, int height) {
        super.mouseHoverEnabled = false;
        super.installUI(c);
        //カラーとホバーの設定
        super.mouseHoverEnabled = true;
        super.setColorMouseHoverNormalButton(mainColor.brighter());
        super.background = mainColor;
        c.setBackground(super.background);
        if (super.mouseHoverEnabled) {
            c.addMouseListener(
                    MaterialUIMovement.getMovement(c, this.colorMouseHoverNormalButton)
            );
        }
        //サイズの設定
        c.setPreferredSize(new Dimension(width, height));
        super.borderEnabled = false;

        c.setFont(GeneralFont.mainFont(c));

    }

    abstract public void installUI(JComponent c);
}
