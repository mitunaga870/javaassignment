package ui.button;

import java.awt.Dimension;
import javax.swing.*;
import mdlaf.animation.MaterialUIMovement;
import mdlaf.components.button.MaterialButtonUI;
import mdlaf.utils.MaterialColors;


/**.
 * 購入ボタンなどのSubmitボタンのUIを設定するクラス
 */
public class Submit extends MaterialResponsiveButtonUI {
    //The propriety order inside the method installUI is important
    //because some propriety should be override
    @Override
    public void installUI(JComponent c) {
        setUI(c, MaterialColors.BLUE_200, reactive.widhtPer(15), reactive.heightPer(10));
    }

    public Submit(int maxW, int maxH) {
        super(maxW, maxH);
    }
}
