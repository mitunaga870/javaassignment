package ui.button;

import mdlaf.utils.MaterialColors;

import javax.swing.*;

public class NonCustomerButton extends MaterialResponsiveButtonUI{
    public NonCustomerButton(int maxW, int maxH) {
        super(maxW, maxH);
    }

    @Override
    public void installUI(JComponent c) {
        setUI(c, MaterialColors.GRAY_100,reactive.widhtPer(10),reactive.heightPer(5));
    }
}
