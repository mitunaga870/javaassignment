package ui.button;

import java.awt.Dimension;
import javax.swing.JComponent;
import mdlaf.utils.MaterialColors;

public class OrderLIstButtonUI extends ui.button.MaterialResponsiveButtonUI {
    public OrderLIstButtonUI(int maxW, int maxH) {
        super(maxW, maxH);
    }

    @Override
    public void installUI(JComponent c) {
        setUI(c, MaterialColors.GRAY_400,reactive.widhtPer(100), reactive.heightPer(30));
        c.setMaximumSize(new Dimension(reactive.widhtPer(100), reactive.heightPer(30)));
        super.setColorMouseHoverNormalButton(MaterialColors.RED_400);
    }
}
