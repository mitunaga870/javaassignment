package ui.button;

import mdlaf.utils.MaterialColors;

import javax.swing.*;

public class SettingAddButton extends ui.button.MaterialResponsiveButtonUI {
    private boolean isSub;

    public SettingAddButton(int maxW, int maxH) {
        super(maxW, maxH);
        isSub = false;
    }

    @Override
    public void installUI(JComponent c) {
        setUI(c, MaterialColors.GRAY_100, reactive.widhtPer(15), 25);
    }
}
