package ui.button;

import enums.FoodGenreTypeClass;
import foods.Food;
import javax.swing.JComponent;
import javax.swing.plaf.ButtonUI;
import mdlaf.utils.MaterialColors;

public class MenuButtonUI extends MaterialResponsiveButtonUI {
    Food food;
    public MenuButtonUI(int maxW, int maxH) {
        super(maxW, maxH);
        food = null;
    }

    @Override
    public void installUI(JComponent c) {
        switch (food.getType()) {
            case FoodGenreTypeClass.FoodGenreType.Tonkotu -> {
                setUI(c, MaterialColors.RED_400, reactive.widhtPer(22), reactive.heightPer(40));
            }
            case FoodGenreTypeClass.FoodGenreType.Shoyu -> {
                setUI(c, MaterialColors.ORANGE_400, reactive.widhtPer(22), reactive.heightPer(40));
            }
            case FoodGenreTypeClass.FoodGenreType.Saltflavoured -> {
                setUI(c, MaterialColors.YELLOW_400, reactive.widhtPer(22), reactive.heightPer(40));
            }
            default -> {
                setUI(c, MaterialColors.BLUE_200, reactive.widhtPer(22), reactive.heightPer(40));
            }
        }
    }

    public ButtonUI setFood(Food food) {
        this.food = food;
        return this;
    }
}
