package ui.button;

import component.FontCustomedLabel;
import java.awt.*;
import javax.swing.*;
import ui.GeneralFont;

/**.
 * ホーム画面のメインボタン
 */
public class HomeMain extends MaterialResponsiveButtonUI {
    private Color mainColor;
    private ImageIcon icon;

    @Override
    public void installUI(JComponent c) {
        if (mainColor == null)
            return;
        setUI(c,mainColor,reactive.widhtPer(30),reactive.heightPer(70));

        if (icon != null) {
            c.setLayout(new BoxLayout(c, BoxLayout.Y_AXIS));
            c.add(Box.createRigidArea(new Dimension(0, reactive.heightPer(20))));
            icon = new ImageIcon(icon.getImage().getScaledInstance(reactive.widhtPer(20), reactive.widhtPer(20), Image.SCALE_SMOOTH));
            JLabel label = new JLabel(icon);
            label.setAlignmentX(Component.CENTER_ALIGNMENT);
            c.add(label);
            FontCustomedLabel text = new FontCustomedLabel(((JButton) c).getText());
            text.setAlignmentX(Component.CENTER_ALIGNMENT);
            text.setFont(GeneralFont.mainFont(c));
            c.add(text);
            ((JButton) c).setText("");
        }
    }

    public void setImage(String path) {
        this.icon = new ImageIcon(path);
    }

    public void installUI(JComponent c, Color mainColor) {
        setUI(c,mainColor,reactive.widhtPer(30),reactive.heightPer(70));
    }

    public HomeMain(int maxW, int maxH) {
        super(maxW, maxH);
        this.mainColor = null;
    }

    public HomeMain set(Color mainColor, String imagePath) {
        this.mainColor = mainColor;
        this.icon = new ImageIcon(imagePath);
        return this;
    }
}
