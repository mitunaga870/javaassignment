package ui.button;

import component.FontCustomedLabel;
import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.ButtonUI;
import mdlaf.animation.MaterialUIMovement;
import mdlaf.components.button.MaterialButtonUI;
import mdlaf.utils.MaterialColors;
import ui.GeneralFont;

/**.
 * ホーム画面のサブボタンの設定
 */
public class HomeSub extends MaterialResponsiveButtonUI {
    private ImageIcon icon;
    public HomeSub(int maxW, int maxH) {
        super(maxW, maxH);
    }

    @Override
    public void installUI(JComponent c) {
        setUI(c, MaterialColors.BLUE_200, reactive.widhtPer(15), reactive.heightPer(30));
        if (icon != null) {
            c.setLayout(new BoxLayout(c, BoxLayout.Y_AXIS));
            c.add(Box.createRigidArea(new Dimension(0, reactive.heightPer(5))));
            icon = new ImageIcon(icon.getImage().getScaledInstance(reactive.widhtPer(10), reactive.widhtPer(10), Image.SCALE_SMOOTH));
            FontCustomedLabel label = new FontCustomedLabel(icon);
            label.setAlignmentX(Component.CENTER_ALIGNMENT);
            c.add(label);
            FontCustomedLabel text = new FontCustomedLabel(((JButton) c).getText());
            text.setAlignmentX(Component.CENTER_ALIGNMENT);
            text.setFont(GeneralFont.mainFont(text));
            c.add(text);
            ((JButton) c).setText("");
        }
    }

    public HomeSub set(String imagePath) {
        this.icon = new ImageIcon(imagePath);
        return this;
    }
}
