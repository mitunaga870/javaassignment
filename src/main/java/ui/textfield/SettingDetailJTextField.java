package ui.textfield;

import javax.swing.*;
import javax.swing.plaf.TextUI;

public class SettingDetailJTextField extends MaterialResponsiveTextFieldUI {
    private boolean isSub;
    public SettingDetailJTextField(int maxWidth, int maxHeight) {
        super(maxWidth, maxHeight);
    }

    public SettingDetailJTextField(SettingDetailJTextField settingDetailJTextField) {
        super(settingDetailJTextField);
    }

    @Override
    public void installUI(JComponent c) {
        if(isSub) {
            setUI(c, reactive.widhtPer(35),25);
        } else {
            setUI(c, reactive.widhtPer(70), 25);
        }
    }

    public TextUI setSub() {
        SettingDetailJTextField result = new SettingDetailJTextField(this);
        result.isSub = true;
        return result;
    }

    public SettingDetailJTextField setMain() {
        SettingDetailJTextField result = new SettingDetailJTextField(this);
        result.isSub = false;
        return result;
    }
}
