package ui.textfield;

import java.awt.Color;
import mdlaf.components.textfield.MaterialTextFieldUI;
import ui.Reactive;

public abstract class MaterialResponsiveTextFieldUI extends MaterialTextFieldUI {
    protected Reactive reactive;

    public MaterialResponsiveTextFieldUI(int maxWidth, int maxHeight) {
        super();
        reactive = new Reactive(maxWidth, maxHeight);
        this.reactive = reactive;
    }

    public MaterialResponsiveTextFieldUI(MaterialResponsiveTextFieldUI base) {
        this.reactive = base.reactive;
    }

    public void setUI(javax.swing.JComponent c, int width, int height) {
        c.setPreferredSize(new java.awt.Dimension(width, height));
        super.installUI(c);

        c.setForeground(Color.BLACK);
    }

    abstract public void installUI(javax.swing.JComponent c);
}
