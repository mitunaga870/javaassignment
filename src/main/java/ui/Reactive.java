package ui;

public class Reactive {
    private int MaxWidth;
    private int MaxHeight;

    public Reactive(int MaxWidth, int MaxHeight) {
        this.MaxWidth = MaxWidth - 20;
        this.MaxHeight = MaxHeight - 20;
    }

    public int widhtPer(int i) {
        return (int) (MaxWidth * (i / 100.0));
    }

    public int heightPer(int i) {
        return (int) (MaxHeight * (i / 100.0));
    }
}
