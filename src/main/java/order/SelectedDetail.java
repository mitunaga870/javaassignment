package order;

public class SelectedDetail {
    private String detailName;
    private String optionName;
    private int price;

    /**.
     * コンストラクタ
     * @param detailName 詳細名
     * @param optionName オプション名
     * @param price 価格
     */
    public SelectedDetail(String detailName, String optionName, int price) {
        this.detailName = detailName;
        this.optionName = optionName;
        this.price = price;
    }

    /**.
     * コンストラクタ
     * そのカスタムをしないときに利用
     * @param detailName 詳細名
     */
    public SelectedDetail(String detailName) {
        this.detailName = detailName;
        this.optionName = "none";
        this.price = 0;
    }

    public int getPrice() {
        return this.price;
    }

    /**.
     * detailNameとoptionNameが同じならtrueを返す
     */
    public boolean equals(SelectedDetail selectedDetail) {
        return (
            this.detailName.equals(selectedDetail.detailName) &&
                this.optionName.equals(selectedDetail.optionName)
        );
    }

    public String getDetailName() {
        return this.detailName;
    }

    public String getOptionName() {
        return this.optionName;
    }
}
