package order;

import java.util.ArrayList;

/**
 * 注文の詳細のリストを表すクラス
 */
public class SelectedDetailList {
    private ArrayList<SelectedDetail> selectedDetails;

    public SelectedDetailList() {
        this.selectedDetails = new ArrayList<SelectedDetail>();
    }

    public int getTotalPrice() {
        int total = 0;
        for (SelectedDetail selectedDetail : this.selectedDetails) {
            total += selectedDetail.getPrice();
        }
        return total;
    }

    public void add(SelectedDetail button) {
        this.selectedDetails.add(button);
    }

    public boolean equals(SelectedDetailList selectedDetailList) {
        for (SelectedDetail selectedDetail : selectedDetailList.selectedDetails) {
            if (!this.contains(selectedDetail)) {
                return false;
            }
        }
        return true;
    }

    private boolean contains(SelectedDetail selectedDetail) {
        for (SelectedDetail detail : this.selectedDetails) {
            if (detail.equals(selectedDetail)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<SelectedDetail> getArrayList() {
        return this.selectedDetails;
    }
}
