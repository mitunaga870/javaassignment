package order;

import enums.DependencyTypeClass.DependencyType;
import enums.FoodGenreTypeClass;
import foods.Dependency;
import foods.Food;
import foods.FoodList;
import java.util.ArrayList;

/**.
 * 注文リストを表すクラス
 */
public class OrderList {
    private ArrayList<OrderFood> foods;
    private ArrayList<OrderFood> usedFoods; //dependenciesを使った食品のリスト

    public OrderList() {
        this.foods = new ArrayList<OrderFood>();
    }

    /**.
     * 品目の種類数を返す
     */
    public int getLenOfItem() {
        int count = 0;
        ArrayList<OrderFood> list = new ArrayList<>();
        for (OrderFood food : this.foods) {
            if (!list.contains(food)) {
                list.add(food);
                count++;
            }
        }
        return count;
    }

    /**.
     * 重複なしの食品リストを返す
     */

    public int getTotalPrice() {
        int total = 0;
        for (OrderFood food : this.foods) {
            total += food.getPrice();
        }
        return total;
    }

    public void add(OrderFood food) {
        if (this.contains(food)) {
            for (OrderFood f : this.foods) {
                if (f.equals(food)) {
                    f.amount++;
                }
            }
            return;
        }
        this.foods.add(food);
    }

    public void clear() {
        this.foods.clear();
    }

    public ArrayList<OrderFood> getArrayList() {
        return this.foods;
    }
    /**.
     * 重複ありのリストを返す
     * @return ArrayList 重複ありのリスト
     */
    public ArrayList<OrderFood> getDupArrayList() {
        ArrayList<OrderFood> result = new ArrayList<>();
        for (OrderFood food : this.foods) {
            for (int i = 0; i < food.amount; i++) {
                OrderFood newFood = new OrderFood(food);
                newFood.amount = 1;
                result.add(newFood);
            }
        }
        return result;
    }

    /**.
     * 依存関係を満たしているかを確認する
     * @return 満たしているか
     */
    public boolean check() {
        //Check dependencies
        //全てのメニューに対して
        usedFoods = new ArrayList<OrderFood>();
        for (OrderFood food : this.getDupArrayList()) {
            //依存関係がある場合
            if (food.hasDependency()) {
                //依存する食品が注文リストに含まれているか確認
                Boolean flag = false; //依存する食品が注文リストに含まれているかのフラグ
                for (Dependency dependency : food.getDependencies()) {
                    OrderFood usefood;
                    //依存がGenreかIDかで処理を分ける
                    if (dependency.getDependencyType() == DependencyType.FoodGenreType && this.contains(FoodGenreTypeClass.getFoodGenreType(dependency.get()))) {
                        usefood = this.get(FoodGenreTypeClass.getFoodGenreType(dependency.get()));
                    } else if (dependency.getDependencyType() == DependencyType.FoodId && this.contains(dependency.get())) {
                        usefood = this.get(dependency.get());
                    } else {
                        continue;
                    }
                    if (!usedFoods.contains(usefood)) {
                        usedFoods.add(usefood);
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    return false;
                }
            }
        }
        return true;
    }

    public OrderFood get(int rowIndex) {
        return this.foods.get(rowIndex);
    }

    /**.
     * 指定したIDの食品を返す
     * @param id 食品ID
     * @return 食品
     */
    public OrderFood get(String id) {
        for (OrderFood food : this.foods) {
            if (food.getId().equals(id)) {
                return food;
            }
        }
        return null;
    }

    public OrderFood get(FoodGenreTypeClass.FoodGenreType type) {
        for (OrderFood food : this.foods) {
            if (food.getType() == type) {
                return food;
            }
        }
        return null;
    }

    /**.
     * 指定した食品を注文リストから削除する
     * @param food 食品
     */
    public void remove(OrderFood food) {
        while (this.foods.contains(food)) {
            this.foods.remove(food);
        }
    }

    /**.
     * 指定したIDの食品が注文リストに含まれているかを返す
     * @param s 食品ID
     * @return 含まれているか
     */
    public boolean contains(String s) {
        for (OrderFood food : this.foods) {
            if (food.getId().equals(s)) {
                return true;
            }
        }
        return false;
    }

    private boolean contains(OrderFood food) {
        for (OrderFood f : this.foods) {
            if (f.equals(food)) {
                return true;
            }
        }
        return false;
    }

    /**.
     * 指定したGenreの食品が注文リストに含まれているかを返す
     * @param type Genre
     * @return 含まれているか
     */
    public boolean contains(FoodGenreTypeClass.FoodGenreType type) {
        for (OrderFood food : this.foods) {
            if (food.getType() == type) {
                return true;
            }
        }
        return false;
    }
}
