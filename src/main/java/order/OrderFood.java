package order;

import enums.FoodGenreTypeClass;
import foods.Food;

public class OrderFood extends Food {
    private SelectedDetailList selectedDetailList;
    public int amount;
    /**
     * .
     * コンストラクタ
     *
     * @param food 食品
     */
    public OrderFood(Food food) {
        super(food.getId(), food.getName(), food.getPrice(), food.getType(), food.getDependencies());
        this.amount = 1;
        this.selectedDetailList = new SelectedDetailList();

    }

    @Override
    public int getPrice() {
        int price = super.getPrice();
        price += this.selectedDetailList.getTotalPrice();
        return price * this.amount;
    }

    public void setSelectedDetail(SelectedDetailList selectedDetailList) {
        this.selectedDetailList = selectedDetailList;
    }


    /**
     * idとselectedDetailListが同じならtrueを返す
     */
    public Boolean equals(OrderFood food) {
        return (
            this.getId().equals(food.getId()) &&
                this.selectedDetailList.equals(food.selectedDetailList)
        );
    }

    public String getDetailText() {
        String text = "";
        for (SelectedDetail selectedDetail : this.selectedDetailList.getArrayList()) {
            text += selectedDetail.getDetailName() + " : " + selectedDetail.getOptionName() + "   " + selectedDetail.getPrice() + "yen,     \n";

        }
        return text;
    }

    public String[] getDetailTexts() {
        String[] texts = new String[this.selectedDetailList.getArrayList().size()];
        for (int i = 0; i < this.selectedDetailList.getArrayList().size(); i++) {
            SelectedDetail selectedDetail = this.selectedDetailList.getArrayList().get(i);
            texts[i] = selectedDetail.getDetailName() + " : " + selectedDetail.getOptionName() + "   " + selectedDetail.getPrice() + "yen";
        }
        return texts;
    }
}
