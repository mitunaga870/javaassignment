package foods;

import enums.DependencyTypeClass.DependencyType;
import enums.FoodGenreTypeClass;
import java.util.ArrayList;
import java.util.UUID;
import javax.swing.JButton;

/**.
 * 食品クラス
 */
public class Food {
    private String id;
    private String name;
    private int price;
    private ArrayList<Dependency> dependencies;
    private DetailList details;
    protected FoodGenreTypeClass.FoodGenreType type;

    /**
     * .
     * コンストラクタ
     *
     * @param name  食品名
     * @param price 価格
     */
    public Food(String id, String name, int price, FoodGenreTypeClass.FoodGenreType type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.dependencies = new ArrayList<Dependency>();
        this.details = new DetailList();
        this.type = type;
    }

    public Food(String id, String name, int price, FoodGenreTypeClass.FoodGenreType type, ArrayList<Dependency> dependencies) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.dependencies = dependencies;
        this.details = new DetailList();
        this.type = type;
    }

    public Food(FoodGenreTypeClass.FoodGenreType type) {
        this.id = UUID.randomUUID().toString();
        this.name = "新規メニュー";
        this.price = 0;
        this.dependencies = new ArrayList<Dependency>();
        this.details = new DetailList();
        this.type = type;
    }


    public String getName() {
        return this.name;
    }

    public int getPrice() {
        return this.price;
    }


    /**.
     * 詳細を追加する
     * @param detailName
     * @param option
     * @param price
     */
    public void addDetail(String detailName, String option, int price) {
        if (details.contains(detailName)) {
            details.get(detailName).addOption(option, price);
        } else {
            Detail detail = new Detail(detailName, option, price);
            details.add(detail);
        }
        System.out.println(details);
    }

    public void addDetail(Detail detail) {
        if (details.contains(detail.getName())) {
            details.get(detail.getName()).addOption(detail.getOptions());
        } else {
            details.add(detail);
        }
    }

    public String getId() {
        return this.id;
    }

    public DetailList getDetails() {
        return this.details;
    }

    public Boolean hasDependency() {
        return this.dependencies.size() > 0;
    }

    public ArrayList<Dependency> getDependencies() {
        return this.dependencies;
    }

    public FoodGenreTypeClass.FoodGenreType getGenre() {
        return type;
    }

    public FoodGenreTypeClass.FoodGenreType getType() {
        return this.type;
    }

    public void addDependency(DependencyType dependencyType, String dependencyId) {
        this.dependencies.add(new Dependency(dependencyType, dependencyId));
    }

    public void addDependency(Dependency dependency) {
        this.dependencies.add(dependency);
    }

    public void setName(String text) {
        this.name = text;
    }

    public void clearDetails() {
        this.details.clear();
    }

    public void setPrice(int i) {
        this.price = i;
    }

    public void clearDependencies() {
        this.dependencies.clear();
    }
    public String getFoodId() {
        return this.id;
    }

    public void setType(FoodGenreTypeClass.FoodGenreType selectedGenre) {
        this.type = selectedGenre;
    }

    public String getImagePath() {
        String path = this.id + ".jfif";
        String path2 = this.id + ".jpg";
        String path3 = this.id + ".png";

        if (new java.io.File(path).exists())
            return path;
        else if (new java.io.File(path2).exists())
            return path2;
        else if (new java.io.File(path3).exists())
            return path3;
        else {
            return "NoImage.png";
        }
    }
}
