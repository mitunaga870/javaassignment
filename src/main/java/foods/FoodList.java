package foods;

import java.util.ArrayList;

/**.
 * 食料品のリストを表す抽象クラス
 */
public abstract class FoodList {
    protected ArrayList<Food> foods;

    public FoodList() {
        foods = new ArrayList<Food>();
    }

    public FoodList(ArrayList<Food> foods) {
        this.foods = foods;
    }

    /**.
     * 食料品のリストを取得する
     * @return 食料品のリスト
     */
    public Food get(String id) {
        for (Food food : foods) {
            if (food.getId().equals(id)) {
                return food;
            }
        }
        return null;
    }

    /**.
     * 食品を追加する
     */
    public void add(Food food) {
        foods.add(food);
    }

    /**.
     * 食品を削除する
     * @param food 削除する食品
     */
    public void remove(Food food) {
        while (foods.contains(food)) {
            foods.remove(food);
        }
    }

    /**.
     * 食品を削除する
     * @param id 削除する食品のID
     */
    public void remove(String id) {
        for (Food food : foods) {
            if (food.getId().equals(id)) {
                foods.remove(food);
                break;
            }
        }
    }

    /**.
     * 食品のリストのサイズを取得する
     * @return 食品のリストのサイズ
     */
    public int size() {
        return foods.size();
    }

    /**.
     * 食品のリストを取得する
     * @return 食品のリスト - ArrayList<Food>
     */
    public ArrayList<Food> getFoods() {
        return foods;
    }

    public void clear() {
        foods.clear();
    }

    public Boolean contains(Food food) {
        return foods.contains(food);
    }

    public Boolean contains(String id) {
        for (Food food : foods) {
            if (food.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }
}
