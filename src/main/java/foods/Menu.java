package foods;

import enums.FoodGenreTypeClass.FoodGenreType;
import java.util.ArrayList;

/**.
 * メニューを表すクラス
 */
public class Menu extends FoodList {
    public Menu(FoodGenreType genreType) {
        super();
        foodGenreType = genreType;
    }

    public Menu(ArrayList<Food> foods) {
        super(foods);
    }

    protected FoodGenreType foodGenreType;

    public FoodGenreType genreType() {
        return foodGenreType;
    }
}
