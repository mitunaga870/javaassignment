package foods;

/**.
 * 注文の詳細を格納するクラス
 */
public class Detail {
    private String Name;
    private Option options;

    /**.
     * コンストラクタ
     * @param name 食品名
     */
    public Detail(String name, String option, int price) {
        this.Name = name;
        this.options = new Option();
        this.options.put(option, price);
    }

    public Detail() {
        this.Name = "";
        this.options = new Option();
    }

    /**.
     * オプションを追加する
     * @param option オプション
     */
    public void addOption(String option, int price) {
        this.options.put(option, price);
    }

    public String getName() {
        return this.Name;
    }
    public void setName(String name) {
        this.Name = name;
    }

    public Option getOptions() {
        return this.options;
    }

    public void addOption(Option options) {
        this.options.put(options);
    }

    public void clearOptions() {
        this.options.clear();
    }
}
