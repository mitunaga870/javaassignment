package foods;

import enums.FoodGenreTypeClass;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JPanel;

/**.
 * メニューパネルをまとめたクラス
 */
public class MenuPanel {
    public ArrayList<JButton> dessert;
    public ArrayList<JButton> drink;
    public ArrayList<JButton> rice;
    public ArrayList<JButton> saltflavoured;
    public ArrayList<JButton> set;
    public ArrayList<JButton> shoyu;
    public ArrayList<JButton> side;
    public ArrayList<JButton> tonkotu;
    public ArrayList<JButton> topping;


    /**.
     * Constractor<br>
     * @param dessert
     * @param drink
     * @param rice
     * @param saltflavoured
     * @param set
     * @param shoyu
     * @param side
     * @param tonkotu
     * @param topping
     */
    public MenuPanel(ArrayList<JButton> dessert, ArrayList<JButton> drink, ArrayList<JButton> rice, ArrayList<JButton> saltflavoured, ArrayList<JButton> set, ArrayList<JButton> shoyu, ArrayList<JButton> side, ArrayList<JButton> tonkotu, ArrayList<JButton> topping) {
        this.dessert = dessert;
        this.drink = drink;
        this.rice = rice;
        this.saltflavoured = saltflavoured;
        this.set = set;
        this.shoyu = shoyu;
        this.side = side;
        this.tonkotu = tonkotu;
        this.topping = topping;
    }

    /**.
     * メニューのパネルを取得する
     * @param type
     * @return
     */
    public ArrayList<JButton> getButton(FoodGenreTypeClass.FoodGenreType type) {
        switch (type) {
            case Dessert -> {
                return dessert;
            }
            case Drink -> {
                return drink;
            }
            case Rice -> {
                return rice;
            }
            case Saltflavoured -> {
                return saltflavoured;
            }
            case Set -> {
                return set;
            }
            case Shoyu -> {
                return shoyu;
            }
            case Side -> {
                return side;
            }
            case Tonkotu -> {
                return tonkotu;
            }
            case Topping -> {
                return topping;
            }
            default -> {
                return null;
            }
        }
    }
}
