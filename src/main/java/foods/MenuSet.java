package foods;

import enums.FoodGenreTypeClass;

/**.
 * メニューをまとめたクラス
 */
public class MenuSet {
    public Menu dessert;
    public Menu drink;
    public Menu rice;
    public Menu saltflanvored;
    public Menu shoyu;
    public Menu side;
    public Menu tonkotu;
    public Menu topping;
    public Menu set;

    /**.
     * Constractor<br
     * @param dessert
     * @param drink
     * @param rice
     * @param saltflanvored
     * @param set
     * @param shoyu
     * @param side
     * @param tonkotu
     * @param topping
     */
    public MenuSet(Menu dessert, Menu drink, Menu rice, Menu saltflanvored, Menu set, Menu shoyu, Menu side, Menu tonkotu, Menu topping) {
        this.dessert = dessert;
        this.drink = drink;
        this.rice = rice;
        this.saltflanvored = saltflanvored;
        this.set = set;
        this.shoyu = shoyu;
        this.side = side;
        this.tonkotu = tonkotu;
        this.topping = topping;
    }

    /**.
     * メニューを取得する
     * @param type メニューの種類
     * @return メニュー
     */
    public Menu getMenu(FoodGenreTypeClass.FoodGenreType type) {
        switch (type) {
            case Dessert -> {
                return dessert;
            }
            case Drink -> {
                return drink;
            }
            case Rice -> {
                return rice;
            }
            case Saltflavoured -> {
                return saltflanvored;
            }
            case Set -> {
                return set;
            }
            case Shoyu -> {
                return shoyu;
            }
            case Side -> {
                return side;
            }
            case Tonkotu -> {
                return tonkotu;
            }
            case Topping -> {
                return topping;
            }
            default -> {
                return null;
            }
        }
    }
}
