package foods;

import enums.DependencyTypeClass.DependencyType;
import enums.FoodGenreTypeClass;
import enums.FoodGenreTypeClass.FoodGenreType;

/**.
 * 品物が必要とする品物の情報を格納するクラス
 */
public class Dependency {
    private FoodGenreType type;
    private String foodId;
    private DependencyType dependencyType;

    public Dependency(FoodGenreType type) {
        this.type = type;
        this.dependencyType = DependencyType.FoodGenreType;
    }

    public Dependency(String foodId) {
        this.foodId = foodId;
        this.dependencyType = DependencyType.FoodId;
    }

    /**.
     * コンストラクタ
     * @param dependencyType 依存する食品の種類
     * @param dependencyId 依存する食品のIDまたはGenre
     */
    public Dependency(DependencyType dependencyType, String dependencyId) {
        this.dependencyType = dependencyType;
        if (dependencyType == DependencyType.FoodGenreType) {
            this.type = FoodGenreTypeClass.getFoodGenreType(dependencyId);
        } else {
            this.foodId = dependencyId;
        }
    }

    public DependencyType getDependencyType() {
        return this.dependencyType;
    }

    /**.
     * 依存する食品のIDまたはGenreを文字列でを返す
     * @return 依存する食品のID
     */
    public String get() {
        if (this.dependencyType == DependencyType.FoodGenreType) {
            return this.type.getString();
        } else {
            return this.foodId;
        }
    }

    public void setDependencyType(DependencyType dependencyType) {
        this.dependencyType = dependencyType;
    }

    public void set(FoodGenreType genre) {
        if (this.dependencyType != DependencyType.FoodGenreType) {
            throw new IllegalArgumentException("DependencyTypeがFoodGenreTypeではありません");
        }
        this.type = genre;
    }
    public void set(String foodId) {
        if (this.dependencyType != DependencyType.FoodId) {
            throw new IllegalArgumentException("DependencyTypeがFoodIdではありません");
        }
        this.foodId = foodId;
    }
}
