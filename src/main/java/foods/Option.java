package foods;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class Option implements Map<String, Integer>{
    ArrayList<String> options;
    ArrayList<Integer> prices;

    public Option() {
        super();
        this.options = new ArrayList<String>();
        this.prices = new ArrayList<Integer>();
    }
    @Override
    public int size() {
        return options.size();
    }

    @Override
    public boolean isEmpty() {
        return options.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return options.contains(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return prices.contains(value);
    }

    @Override
    public Integer get(Object key) {
        return prices.get(options.indexOf(key));
    }

    @Nullable
    @Override
    public Integer put(String key, Integer value) {
        options.add(key);
        prices.add(value);
        return value;
    }

    @Override
    public Integer remove(Object key) {
        prices.remove(options.indexOf(key));
        options.remove(key);
        return null;
    }

    @Override
    public void putAll(@NotNull Map<? extends String, ? extends Integer> m) {
        for (String key : m.keySet()) {
            options.add(key);
            prices.add(m.get(key));
        }
    }

    @Override
    public void clear() {
        options.clear();
        prices.clear();
    }

    @NotNull
    @Override
    public Set<String> keySet() {
        Set<String> set = new HashSet<String>();
        for (String key : options) {
            set.add(key);
        }
        return set;
    }

    @NotNull
    @Override
    public Collection<Integer> values() {
        return prices;
    }

    @NotNull
    @Override
    public Set<Entry<String, Integer>> entrySet() {
        Set<Entry<String, Integer>> set = new HashSet<Entry<String, Integer>>();
        for (int i = 0; i < options.size(); i++) {
            int finalI = i;
            set.add(new Entry<String, Integer>() {
                @Override
                public String getKey() {
                    return options.get(finalI);
                }

                @Override
                public Integer getValue() {
                    return prices.get(finalI);
                }

                @Override
                public Integer setValue(Integer value) {
                    prices.set(finalI, value);
                    return value;
                }
            });
        }
        return set;
    }

    public Map<String, Integer>[] get() {
        return new Map[]{this};
    }

    public void put(Option options) {
        for (String key : options.keySet()) {
            this.put(key, options.get(key));
        }
    }
}
