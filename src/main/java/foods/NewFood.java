package foods;

import enums.FoodGenreTypeClass;

public class NewFood extends Food{
    public NewFood() {
        super(null);
    }

    public Food downcast(){
        throw new RuntimeException("NewFoodはdowncastできません");
    }

    public Food ChangeFood(FoodGenreTypeClass.FoodGenreType type){
        settype(type);
        return this;
    }

    private void settype(FoodGenreTypeClass.FoodGenreType type){
        this.type = type;
    }
}
