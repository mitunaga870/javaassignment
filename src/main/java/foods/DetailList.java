package foods;

import java.util.ArrayList;

public class DetailList {
    public ArrayList<Detail> details;

    DetailList() {
        details = new ArrayList<Detail>();
    }

    public Detail get(String name) {
        for (Detail detail : details) {
            if (detail.getName().equals(name)) {
                return detail;
            }
        }
        return null;
    }

    public void add(Detail detail) {
        details.add(detail);
    }

    public void remove(Detail detail) {
        while (details.contains(detail)) {
            details.remove(detail);
        }
    }

    public void remove(String name) {
        for (Detail detail : details) {
            if (detail.getName().equals(name)) {
                details.remove(detail);
                break;
            }
        }
    }

    public Boolean contains(Detail detail) {
        return details.contains(detail);
    }

    public Boolean contains(String name) {
        for (Detail detail : details) {
            if (detail.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public int size() {
        return details.size();
    }

    public ArrayList<Detail> get() {
        return details;
    }

    public void clear() {
        details.clear();
    }
}
