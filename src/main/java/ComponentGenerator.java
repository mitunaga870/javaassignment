import component.*;
import enums.FoodGenreTypeClass;
import foods.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.*;

import foods.Menu;
import order.OrderFood;
import ui.Reactive;
import ui.button.MenuButtonUI;
import ui.button.OrderLIstButtonUI;
import ui.button.SettingAddButton;
import ui.textfield.SettingDetailJTextField;

/**.
 * コンポーネント生成を行うクラス
 * CreateMenus: メニューセットからメニューを生成する
 * CreateMenu: メニューから食品を生成する
 * CreateFoodPanel: 食品からパネルを生成する
 */
public class ComponentGenerator {
    Main main;

    /**
     * .
     * コンストラクタ
     */
    public ComponentGenerator(Main main) {
        this.main = main;
    }

    /**
     * .
     * メニューセットからメニューを生成する
     *
     * @param menuset メニューセット
     * @return メニューのパネルのリスト
     */
    public MenuPanel createMenus(MenuSet menuset) {
        ArrayList<JButton> dessert = createMenu(menuset.getMenu(FoodGenreTypeClass.FoodGenreType.Dessert));
        ArrayList<JButton> drink = createMenu(menuset.getMenu(FoodGenreTypeClass.FoodGenreType.Drink));
        ArrayList<JButton> rice = createMenu(menuset.getMenu(FoodGenreTypeClass.FoodGenreType.Rice));
        ArrayList<JButton> saltflavoured = createMenu(menuset.getMenu(FoodGenreTypeClass.FoodGenreType.Saltflavoured));
        ArrayList<JButton> set = createMenu(menuset.getMenu(FoodGenreTypeClass.FoodGenreType.Set));
        ArrayList<JButton> shoyu = createMenu(menuset.getMenu(FoodGenreTypeClass.FoodGenreType.Shoyu));
        ArrayList<JButton> side = createMenu(menuset.getMenu(FoodGenreTypeClass.FoodGenreType.Side));
        ArrayList<JButton> tonkotu = createMenu(menuset.getMenu(FoodGenreTypeClass.FoodGenreType.Tonkotu));
        ArrayList<JButton> topping = createMenu(menuset.getMenu(FoodGenreTypeClass.FoodGenreType.Topping));
        return new MenuPanel(dessert, drink, rice, saltflavoured, set, shoyu, side, tonkotu, topping);
    }

    /**
     * .
     * メニューから食品を生成する
     *
     * @param menu メニュー
     * @return 食品のパネルのリスト
     */
    public ArrayList<JButton> createMenu(Menu menu) {
        ArrayList<JButton> panels = new ArrayList<>();
        for (foods.Food food : menu.getFoods()) {
            panels.add(createFoodPanel(food));
        }
        return panels;
    }

    /**
     * .
     * 食品からパネルを生成する
     *
     * @param food 食品
     * @return 食品のパネル
     */
    public JButton createFoodPanel(Food food) {
        JButton button = makeFoodButton(food);
        button.addActionListener(e -> {
            OrderDetailPage orderDetailPage = new OrderDetailPage(main.frame, food);
            OrderFood result = orderDetailPage.showDialog();
            if (result != null) {
                addOrder(result);
            }
        });
        return button;
    }

    private void addOrder(OrderFood food) {
        main.orderFoodList.add(food);
        main.totalPrice.addPrice(food.getPrice());

        main.orderListPlane.removeAll();
        for (OrderFood f : main.orderFoodList.getArrayList()) {

            JButton button = makeFoodButton(f);
            button.addActionListener(e -> {
                if (UserMethods.confirmDialog("Confirm","Are you sure you want to delete this item?")) {
                    main.totalPrice.removePrice(f.getPrice());
                    main.orderFoodList.remove(f);
                    button.setVisible(false);
                }
            });
            main.orderListPlane.add(button);
        }
        main.orderListPlane.revalidate();
    }

    private JButton makeFoodButton(Food food) {
        JButton button = new JButton();
        button.setLayout(new BoxLayout(button, BoxLayout.Y_AXIS));
        FontCustomedLabel image = new FontCustomedLabel();
        Reactive reactive = new Reactive(main.frame.getWidth(), main.frame.getHeight());
        ImageIcon icon = new ImageIcon(food.getImagePath());
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(reactive.widhtPer(15),reactive.widhtPer(15) , Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        image.setIcon(icon);
        image.setAlignmentX(FontCustomedLabel.CENTER_ALIGNMENT);
        button.add(image);
        FontCustomedLabel label = new FontCustomedLabel(food.getName());
        label.setAlignmentX(FontCustomedLabel.CENTER_ALIGNMENT);
        button.add(label);
        FontCustomedLabel price = new FontCustomedLabel(food.getPrice() + "yen");
        price.setAlignmentX(FontCustomedLabel.CENTER_ALIGNMENT);
        button.add(price);

        MenuButtonUI ui = new MenuButtonUI(main.frame.getWidth(), main.frame.getHeight());
        button.setUI(ui.setFood(food));


        return button;
    }

    private JButton makeFoodButton(OrderFood food) {
        JButton button = new JButton();
        button.setLayout(new FlowLayout());

        FontCustomedLabel image = new FontCustomedLabel();
        Reactive reactive = new Reactive(main.orderListPlane.getWidth(), main.orderListPlane.getHeight());
        ImageIcon icon = new ImageIcon(food.getImagePath());
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(reactive.heightPer(25),reactive.heightPer(25) , Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        image.setIcon(icon);
        image.setAlignmentX(FontCustomedLabel.CENTER_ALIGNMENT);
        button.add(image);

        JPanel Data = new JPanel();
        Data.setLayout(new BoxLayout(Data, BoxLayout.Y_AXIS));

        FontCustomedLabel label = new FontCustomedLabel(food.getName());
        label.setAlignmentX(FontCustomedLabel.CENTER_ALIGNMENT);
        Data.add(label);

        for (String text : food.getDetailTexts()) {
            FontCustomedLabel detail = new FontCustomedLabel(text);
            detail.setAlignmentX(FontCustomedLabel.CENTER_ALIGNMENT);
            Data.add(detail);
        }

        FontCustomedLabel amount = new FontCustomedLabel("amount: " + ((OrderFood) food).amount);
        amount.setAlignmentX(FontCustomedLabel.CENTER_ALIGNMENT);
        Data.add(amount);

        FontCustomedLabel price = new FontCustomedLabel(food.getPrice() + "yen");
        price.setAlignmentX(FontCustomedLabel.CENTER_ALIGNMENT);
        Data.add(price);

        button.add(Data);

        OrderLIstButtonUI ui = new OrderLIstButtonUI(main.orderListPlane.getWidth(), main.orderListPlane.getHeight());
        button.setUI(ui);

        return button;
    }

    private JButton makeButton(FontCustomedLabel[] texts) {
        JButton button = new JButton();
        button.setLayout(new BoxLayout(button, BoxLayout.Y_AXIS));
        for (FontCustomedLabel text : texts) {
            text.setAlignmentX(FontCustomedLabel.CENTER_ALIGNMENT);
            button.add(text);
        }
        return button;
    }

    public void addMenuSettingSelection(FoodGenreTypeClass.FoodGenreType selectedGenre) {
        Menu menu = main.menuSet.getMenu(selectedGenre);
        main.menuSettingSelection.removeAll();

        for (Food food : menu.getFoods()) {
            main.menuSettingSelection.addItem(food);
        }

        main.menuSettingSelection.revalidate();
    }

    public void addMenuSettingDetail(Food selectedFood) {
        //名前と価格をセット
        main.menuSettingName.setText(selectedFood.getName());
        main.menuSettingName.revalidate();
        main.menuSettingPrice.setText(String.valueOf(selectedFood.getPrice()));
        main.menuSettingPrice.revalidate();
        //詳細と依存関係を削除
        main.menuSettingDetailPanel.removeAll();
        main.menuSettingDetailPanel.repaint();
        main.menuSettingDependencyPanel.removeAll();
        main.menuSettingDependencyPanel.repaint();
        SettingDetailJTextField textFieldui = new SettingDetailJTextField(
                main.menuSettingDetailPanel.getWidth(),
                main.menuSettingDetailPanel.getHeight()
        );
        SettingAddButton buttonui = new SettingAddButton(
                main.menuSettingDetailPanel.getWidth(),
                main.menuSettingDetailPanel.getHeight()
        );
        //詳細をセット
        for (Detail datail : selectedFood.getDetails().get()) {
            main.menuSettingDetailPanel.add(
                    new DetailPanel(datail, textFieldui, buttonui)
            );
        }
        main.menuSettingDetailPanel.revalidate();

        //依存関係をセット
        for (Dependency dependency : selectedFood.getDependencies()) {
            main.menuSettingDependencyPanel.add(
                    new DependencyPanel(dependency, textFieldui, buttonui)
            );
        }
        main.menuSettingDependencyPanel.revalidate();

    }

}
