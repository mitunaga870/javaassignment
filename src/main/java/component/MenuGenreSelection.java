package component;

import enums.FoodGenreTypeClass;
import java.util.Map;
import javax.swing.JComboBox;

/**.
 * メニュー選択用のコンボボックス
 */
public class MenuGenreSelection extends JComboBox {
    private Map<Integer, FoodGenreTypeClass.FoodGenreType> map;
    private int length;

    public MenuGenreSelection() {
        super();
        length = 0;
        map = new java.util.HashMap<Integer,FoodGenreTypeClass.FoodGenreType>();

        init();
    }

    public MenuGenreSelection(String type) {
        super();
        length = 0;
        map = new java.util.HashMap<Integer,FoodGenreTypeClass.FoodGenreType>();

        init();

        this.setSelectedItem(type);
    }

    public void setSelectedGenre(FoodGenreTypeClass.FoodGenreType type) {
        this.setSelectedIndex(type.ordinal());
    }

    public void addItem(FoodGenreTypeClass.FoodGenreType type) {
        super.addItem(type.toString());
        map.put(length, type);
        length++;
    }

    private void init() {
        addItem(FoodGenreTypeClass.FoodGenreType.Tonkotu);
        addItem(FoodGenreTypeClass.FoodGenreType.Shoyu);
        addItem(FoodGenreTypeClass.FoodGenreType.Saltflavoured);
        addItem(FoodGenreTypeClass.FoodGenreType.Rice);
        addItem(FoodGenreTypeClass.FoodGenreType.Side);
        addItem(FoodGenreTypeClass.FoodGenreType.Topping);
        addItem(FoodGenreTypeClass.FoodGenreType.Set);
        addItem(FoodGenreTypeClass.FoodGenreType.Dessert);
        addItem(FoodGenreTypeClass.FoodGenreType.Drink);
    }

    public FoodGenreTypeClass.FoodGenreType getSelectedGenre() {
        return map.get(this.getSelectedIndex());
    }
}
