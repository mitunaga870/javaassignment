package component;

import foods.Food;
import foods.NewFood;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import javax.swing.JComboBox;

/**.
 * メニュー選択用のコンボボックス
 */
public class MenuFoodSelection extends JComboBox {
    private Map<Integer, Food> map;
    private int length;

    /**.
     * コンストラクタ
     */
    public MenuFoodSelection() {
        super();
        length = 0;
        map = new java.util.HashMap<Integer,Food>();
    }

    @Override
    public void removeAll(){
        super.removeAll();
        map.clear();
        length = 0;
        addItem(new NewFood());
    }

    /**.
     * 食品を追加する
     * @param food 食品
     */
    public void addItem(@NotNull Food food) {
        super.addItem(food.getName());
        map.put(length, food);
        length++;
    }

    public Food getSelectedFood() {
        return map.get(this.getSelectedIndex());
    }
}
