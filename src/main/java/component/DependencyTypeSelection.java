package component;

import enums.DependencyTypeClass;
import enums.DependencyTypeClass.DependencyType;

import javax.swing.*;

public class DependencyTypeSelection extends JComboBox<String> {
    DependencyType dependencyType;
    public DependencyTypeSelection() {
        super();
        this.addItem("Genre");
        this.addItem("Food");
    }

    public DependencyTypeSelection(DependencyType dependencyType) {
        super();
        this.dependencyType = dependencyType;
        this.addItem("Genre");
        this.addItem("Food");
        this.setSelectedIndex(dependencyType.ordinal());
    }

    public DependencyType getSelectedDependencyType() {
        return DependencyTypeClass.getDependencyType(this.getSelectedItem().toString());
    }
}
