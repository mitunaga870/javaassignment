package component;

public class PriceLabel extends FontCustomedLabel {
    int price;
    public PriceLabel() {
        super("0yen");
        price = 0;
    }

    public void addPrice(int price) {
        this.price += price;
        setText(this.price + "yen");
    }

    public void removePrice(int price) {
        this.price -= price;
        setText(this.price + "yen");
    }
}
