package component;

import java.awt.FlowLayout;
import javax.swing.*;
import java.util.Map;
import ui.button.SettingAddButton;
import ui.textfield.SettingDetailJTextField;

public class OptionPanel extends JPanel {
    public Map.Entry<String , Integer> option;
    private JTextField optionName;
    private JTextField optionPrice;

    public OptionPanel(Map.Entry<String, Integer> option, SettingDetailJTextField textFieldUI, SettingAddButton buttonUI){
        super();
        this.setLayout(new FlowLayout(FlowLayout.LEFT));
        this.option = option;

        String name = "";
        String price = "";
        if (option != null) {
            name = option.getKey();
            price = String.valueOf(option.getValue());
        }

        //オプション名フィールド
        optionName = new JTextField(name);
        optionName.setUI(textFieldUI.setSub());
        this.add(optionName);
        //オプション価格フィールド
        optionPrice = new JTextField(price);
        optionPrice.setUI(textFieldUI.setSub());
        this.add(optionPrice);
        //オプション削除ボタン
        JButton removeOption = new JButton("remove option");
        removeOption.setUI(buttonUI);
        removeOption.addActionListener( e -> {
            this.setVisible(false);
        });
        this.add(removeOption);
    }

    public String getOptionName() {
        return optionName.getText();
    }

    public int getOptionPrice() {
        return Integer.parseInt(optionPrice.getText());
    }
}
