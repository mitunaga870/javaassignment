package component;

import foods.Detail;

import foods.Option;
import java.awt.Component;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.*;
import ui.button.SettingAddButton;
import ui.textfield.SettingDetailJTextField;

public class DetailPanel extends JPanel {
    private Detail detail;
    private JPanel optionPanels;

    public DetailPanel(Detail detail, SettingDetailJTextField textFieldUI, SettingAddButton buttonUI) {
        super();
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.detail = detail;

        //名前フィールド
        JPanel detailHeader = new JPanel();
        detailHeader.setLayout(new FlowLayout(FlowLayout.LEFT));
        JTextField name = new JTextField(detail.getName());
        name.setAlignmentX(JTextField.CENTER_ALIGNMENT);
        name.setUI(textFieldUI.setSub());
        detailHeader.add(name);
        //オプション追加ボタン
        JButton addOption = new JButton("add option");
        addOption.setUI(buttonUI);
        addOption.addActionListener( e -> {
            optionPanels.add(
                new OptionPanel(
                    null,
                    textFieldUI,
                    buttonUI
                )
            );
            this.revalidate();
        });
        detailHeader.add(addOption);
        //詳細削除ボタン
        JButton removeDetail = new JButton("remove detail");
        removeDetail.setUI(buttonUI);
        removeDetail.addActionListener(e -> {
            this.setVisible(false);
        });
        detailHeader.add(removeDetail);
        this.add(detailHeader);
        //オプションフィールド
        optionPanels = new JPanel();
        optionPanels.setLayout(new BoxLayout(optionPanels, BoxLayout.Y_AXIS));
        for (Map.Entry<String, Integer> option : detail.getOptions().entrySet()) {
            optionPanels.add(
                new OptionPanel(
                    option,
                    textFieldUI,
                    buttonUI
                )
            );
        }

        this.add(optionPanels);

    }

    public Detail getDetail() {
        return detail;
    }

    public String getDetailName() {
        return detail.getName();
    }

    /**.
     * オプションパネルのリストを返す
     * @return オプションパネルのリスト
     */
    public ArrayList<OptionPanel> getOptionPanels() {
        ArrayList<OptionPanel> result = new ArrayList<>();
        for (Component component : optionPanels.getComponents()) {
            if (component.isVisible()) {
                result.add((OptionPanel) component);
            }
        }
        return result;
    }
}
