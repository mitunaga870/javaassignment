package component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import ui.GeneralFont;

public class FontCustomedLabel extends JLabel {
    public FontCustomedLabel(String text) {
        super(text);
        this.setFont(GeneralFont.mainFont(this));
    }

    public FontCustomedLabel () {
        super();
        this.setFont(GeneralFont.mainFont(this));
    }

    public FontCustomedLabel (ImageIcon icon) {
        super(icon);
        this.setFont(GeneralFont.mainFont(this));
    }
}
