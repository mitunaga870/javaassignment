package component;

import enums.DependencyTypeClass;
import enums.FoodGenreTypeClass;
import foods.Dependency;

import java.awt.FlowLayout;
import javax.swing.*;
import ui.button.SettingAddButton;
import ui.textfield.SettingDetailJTextField;

public class DependencyPanel extends JPanel {
    private Dependency dependency;
    private DependencyTypeSelection typeSelection;
    private FoodSelection foodSelection;
    private MenuGenreSelection genreSelection;
    public DependencyPanel(Dependency dependency, SettingDetailJTextField textFieldUI, SettingAddButton buttonUI) {
        super();
        this.dependency = dependency;
        this.setLayout(new FlowLayout(FlowLayout.LEFT));

        if (dependency == null) {
            dependency = new Dependency(FoodGenreTypeClass.FoodGenreType.Tonkotu);
        }

        //タイプコンボボックス
        typeSelection = new DependencyTypeSelection(dependency.getDependencyType());
        typeSelection.addActionListener(e -> {
            if (typeSelection.getSelectedDependencyType() == DependencyTypeClass.DependencyType.FoodId) {
                foodSelection.setVisible(true);
                genreSelection.setVisible(false);
            } else {
                foodSelection.setVisible(false);
                genreSelection.setVisible(true);
            }
        });
        this.add(typeSelection);

        //依存する食品のIDまたはGenreフィールド
        genreSelection = new MenuGenreSelection(dependency.get());
        this.add(genreSelection);

        foodSelection = new FoodSelection(dependency.get());
        this.add(foodSelection);

        if (dependency.getDependencyType() == DependencyTypeClass.DependencyType.FoodId) {
            foodSelection.setVisible(true);
            genreSelection.setVisible(false);
        } else {
            foodSelection.setVisible(false);
            genreSelection.setVisible(true);
        }

        //依存関係削除ボタン
        JButton removeDependency = new JButton("remove dependency");
        removeDependency.setUI(buttonUI);
        removeDependency.addActionListener(e -> {
            this.setVisible(false);
        });
        this.add(removeDependency);
    }

    public Dependency getDependency() {
        return dependency;
    }

    public DependencyTypeClass.DependencyType getDependencyType() {
        return typeSelection.getSelectedDependencyType();
    }

    public FoodGenreTypeClass.FoodGenreType getGenre() {
        return genreSelection.getSelectedGenre();
    }

    public String  getFoodId() {
        return foodSelection.getSelectedFoodId();
    }
}
