package component;

import foods.Food;
import java.util.ArrayList;
import javax.swing.JComboBox;
import sqlite.Selector;

public class FoodSelection extends JComboBox {
    private ArrayList<Food> foods;

    public FoodSelection(String id) {
        super();
        foods = Selector.getFoods();
        int index = 0;
        for (Food food : foods) {
            this.addItem(food.getName());

            index++;
            if (food.getFoodId().equals(id)) {
                this.setSelectedIndex(index);
            }
        }
    }

    public Food getSelectedFood() {
        return foods.get(this.getSelectedIndex());
    }

    public String getSelectedFoodId() {
        return foods.get(this.getSelectedIndex()).getFoodId();
    }
}
