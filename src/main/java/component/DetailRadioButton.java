package component;

import javax.swing.JRadioButton;
import order.SelectedDetail;
import order.SelectedDetailList;

/**.
 * 詳細設定用のラジオボタン
 * パラメータにSelectedDetailを持つ
 */
public class DetailRadioButton extends JRadioButton {
    SelectedDetail value;

    /**.
     * コンストラクタ
     * @param value SelectedDetail
     */
    public DetailRadioButton(String text, SelectedDetail value) {
        super(text);
        this.value = value;
    }

    public SelectedDetail getValue() {
        return this.value;
    }
}
