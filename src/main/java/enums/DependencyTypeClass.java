package enums;

public class DependencyTypeClass {
    public enum DependencyType {
        FoodGenreType("FoodGenreType"),
        FoodId("Food"),
        ;
        private String key;

        private DependencyType(final String key) {
            this.key = key;
        }

        public String getString() {
            return key;
        }
    }

    /**.
     * keyからDependencyTypeを取得する
     * @param key key
     * @return DependencyType
     */
    public static DependencyType getDependencyType(final String key) {
        for (DependencyType type : DependencyType.values()) {
            if (type.getString().equals(key)) {
                return type;
            }
        }
        return null;
    }
}
