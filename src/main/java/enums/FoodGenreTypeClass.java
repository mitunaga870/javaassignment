package enums;

/**.
 * 食品のGenreを示す列挙型
 */
public class FoodGenreTypeClass {
    public static enum FoodGenreType {
        Tonkotu("Ton"),
        Shoyu("Sho"),
        Saltflavoured("Sal"),
        Rice("Ric"),
        Side("Sid"),
        Set("Set"),
        Dessert("Des"),
        Topping("Top"),
        Drink("Dri"),
        ;

        private String key;

        private FoodGenreType(final String key) {
            this.key = key;
        }

        public String getString() {
            return key;
        }
    }

    /**.
     * keyからFoodGenreTypeを取得する
     * @param key key
     * @return FoodGenreType
     */
    public static FoodGenreType getFoodGenreType(final String key) {
        for (FoodGenreType type : FoodGenreType.values()) {
            if (type.getString().equals(key)) {
                return type;
            }
        }
        return null;
    }
}
