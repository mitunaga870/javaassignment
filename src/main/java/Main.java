import component.DependencyPanel;
import component.DetailPanel;
import component.FontCustomedLabel;
import component.MenuFoodSelection;
import component.MenuGenreSelection;
import component.OptionPanel;
import component.PriceLabel;
import enums.DependencyTypeClass;
import enums.FoodGenreTypeClass.FoodGenreType;
import foods.Dependency;
import foods.Detail;
import foods.Food;
import foods.MenuPanel;
import foods.MenuSet;

import java.awt.*;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.*;

import mdlaf.utils.MaterialColors;
import order.OrderList;
import sqlite.Inserter;
import sqlite.Selector;
import ui.GeneralFont;
import ui.button.*;
import ui.tabbedpane.MainUI;
import ui.table.OrderResultTableUI;
import ui.textfield.SettingDetailJTextField;


/**
* メインのGUI
*/
public class Main {
    public static JFrame frame;
    //Component
    private JPanel root;
    private JTabbedPane menu;
    private JPanel tonkotuRoot;
    private JPanel saltflanvoredRoot;
    private JButton orderButton;
    private JPanel mainPage;
    private JPanel homePage;
    private JPanel paymentPage;
    private JButton goTonkotu;
    private JButton goShoyu;
    private JButton goSaltflavored;
    private JButton goRice;
    private JButton goSidemenu;
    private JButton goTopping;
    private JButton goSet;
    private JButton goDessert;
    private JButton goDrink;
    private JPanel shoyuRoot;
    private JPanel riceRoot;
    private JPanel sidemenuRoot;
    private JPanel setRoot;
    private JPanel dessertRoot;
    private JPanel toppingRoot;
    private JPanel drinkRoot;
    private JButton backToHomeButton;
    private JButton confirmPaymentButton;
    private JButton cancelPaymentButton;
    private JButton goSetting;
    private JPanel settingPage;
    private JTabbedPane settingTab;
    private JButton exitButton;
    private JButton saveSettingButton;
    private JButton cancelSettingButton;
    private JPanel cardRoot;
    public JPanel orderListPlane;
    private JTable paymentTable;
    private FontCustomedLabel paymentResultLabel;
    private FontCustomedLabel endTimeDisplay;
    private JButton retunPaymentButton;
    private JPanel generalSettingPanel;
    private JPanel menuSettingPanel;
    private MenuGenreSelection menuSettingGenre;
    public MenuFoodSelection menuSettingSelection;
    public JTextField menuSettingName;
    public JTextField menuSettingPrice;
    private JPasswordField passwordField1;
    private JPasswordField passwordField2;
    public JPanel menuSettingDetailPanel;
    public JPanel menuSettingDependencyPanel;
    private JScrollPane menuSettingDetailScroll;
    private JScrollPane menuSettingDepndencyScroll;
    private JButton addDetailButton;
    private JButton addDependencyButton;
    private JPanel endCardPage;
    public PriceLabel totalPrice;
    private JPanel mainSide;
    private JPanel loadPage;
    private JScrollPane tonkotuScroll;
    private JScrollPane saltfranvoredScroll;
    private JScrollPane shoyuScroll;
    private JScrollPane riceScroll;
    private JScrollPane sideScroll;
    private JScrollPane setScroll;
    private JScrollPane dessertScroll;
    private JScrollPane toppingScroll;
    private JScrollPane DrinkScroll;

    //Variable
    public MenuSet menuSet;
    private ArrayList<JPanel> tonkotuPanelList;
    private ArrayList<JPanel> shoyuPanelList;
    private ArrayList<JPanel> saltflanvoredPanelList;
    private ArrayList<JPanel> ricePanelList;
    private ArrayList<JPanel> sidemenuPanelList;
    private ArrayList<JPanel> toppingPanelList;
    private ArrayList<JPanel> setPanelList;
    private ArrayList<JPanel> dessertPanelList;
    private ArrayList<JPanel> drinkPanelList;
    private CardLayout cardLayout;
    public OrderList orderFoodList;
    private final ComponentGenerator cg;
    private final Timer endTimer;
    private final Timer displayTimer;
    private AtomicInteger count;
    private boolean menuSettingFlag;

    /**.
     *  GUIの初期化
     *  */
    public static void main(String[] args) throws InterruptedException {
        frame = new JFrame("Main");
        //タイトルバー削除
        frame.setUndecorated(true);

        Main main = new Main();
        frame.setContentPane(main.root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setResizable(false);
        //表示
        frame.setVisible(true);
        //ウィンドウフルスクリーン
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        main.cardLayout.show(main.cardRoot, "Load");
        //初期化
        Thread thread = new Thread(() -> {
            //UI
            main.init();
            //ホームページに移動
            main.setUI();
        });
        thread.start();
        thread.join();
        main.goHome();
    }


    Main() {
        //Payment
        //paymentPage.setLayout(new BoxLayout(paymentPage, BoxLayout.Y_AXIS));
        //PanelLayout
        tonkotuRoot.setLayout(new FlowLayout());
        shoyuRoot.setLayout(new FlowLayout());
        saltflanvoredRoot.setLayout(new FlowLayout());
        riceRoot.setLayout(new FlowLayout());
        sidemenuRoot.setLayout(new FlowLayout());
        toppingRoot.setLayout(new FlowLayout());
        setRoot.setLayout(new FlowLayout());
        dessertRoot.setLayout(new FlowLayout());
        drinkRoot.setLayout(new FlowLayout());
        //orderFoodList
        orderFoodList = new OrderList();
        //CardLayout
        cardLayout = (CardLayout) (cardRoot.getLayout());
        //MakeComponentGenerator
        cg = new ComponentGenerator(this);
        //Setting Layout
        menuSettingDetailPanel.setLayout(new BoxLayout(menuSettingDetailPanel, BoxLayout.Y_AXIS));
        menuSettingDependencyPanel.setLayout(new BoxLayout(menuSettingDependencyPanel, BoxLayout.Y_AXIS));
        endTimer = new Timer(3000, e -> {
            init();
            goHome();
        });
        displayTimer = new Timer(1000, e -> {
            endTimeDisplay.setText("Please waiting for " + (3 - count.incrementAndGet()) + " seconds");
        });
        menuSettingDetailScroll.setPreferredSize(
                new Dimension(
                        menuSettingDetailScroll.getWidth(),
                        menuSettingDetailScroll.getHeight()
                )
        );
        menuSettingDepndencyScroll.setPreferredSize(
                new Dimension(
                        menuSettingDepndencyScroll.getWidth(),
                        menuSettingDepndencyScroll.getHeight()
                )
        );

        /*
         * 各種アクション
         * go@PageName: @PageNameに遷移
         * backToHomeButton: ホームに戻る
         * orderButton: 支払いページに移動
         * cancelPaymentButton: 支払いをキャンセル
         * confirmPaymentButton: 支払いを確定
         * exitButton: アプリケーションを終了
         * saveSettingButton: 設定を保存
         * cancelSettingButton: 設定をキャンセル
         * retunPaymentButton: 支払いページからメインページに戻る
         */
        goTonkotu.addActionListener(e -> goMain(FoodGenreType.Tonkotu));
        goShoyu.addActionListener(e -> goMain(FoodGenreType.Shoyu));
        goSaltflavored.addActionListener(e -> goMain(FoodGenreType.Saltflavoured));
        goRice.addActionListener(e -> goMain(FoodGenreType.Rice));
        goSidemenu.addActionListener(e -> goMain(FoodGenreType.Side));
        goTopping.addActionListener(e -> goMain(FoodGenreType.Topping));
        goSet.addActionListener(e -> goMain(FoodGenreType.Set));
        goDessert.addActionListener(e -> goMain(FoodGenreType.Dessert));
        goDrink.addActionListener(e -> goMain(FoodGenreType.Drink));
        goSetting.addActionListener(e -> goSetting());
        backToHomeButton.addActionListener(e -> {
            if (UserMethods.confirmDialog("Confirm Cancel", "Are you sure you want to cancel?")) {
                goHome();
                init();
            }
        });
        orderButton.addActionListener(e -> {
            if (orderFoodList.getLenOfItem() == 0) {
                UserMethods.showError("Error", "Please order something");
                return;
            } else if (!orderFoodList.check()) {
                UserMethods.showError("Error", "Unsatisfied dependencies");
                return;
            }
            goPayment();
        });
        cancelPaymentButton.addActionListener(e -> {
            if (UserMethods.confirmDialog("Confirm Cancel", "Are you sure you want to cancel?")) {
                goHome();
                init();
            }
        });
        confirmPaymentButton.addActionListener(e -> {
            if (UserMethods.confirmDialog("Confirm Payment", "Are you sure you want to pay?")) {
                goEnd();
            }
        });
        exitButton.addActionListener(e -> {
            if (UserMethods.confirmDialog("Confirm Exit", "Are you sure you want to exit?")) {
                frame.dispose();
            }
        });
        cancelSettingButton.addActionListener(e -> {
            if (UserMethods.confirmDialog("Confirm Cancel", "Are you sure you want to cancel?")) {
                goHome();
                init();
            }
        });
        retunPaymentButton.addActionListener(e -> goMain());
        menuSettingGenre.addActionListener(e -> {
            menuSettingFlag = false;
            menuSettingSelection.removeAllItems();
            cg.addMenuSettingSelection(menuSettingGenre.getSelectedGenre());
            menuSettingFlag = true;
        });
        menuSettingSelection.addActionListener(e -> {
            if (menuSettingFlag)
                cg.addMenuSettingDetail(menuSettingSelection.getSelectedFood());
        });
        addDetailButton.addActionListener(e -> {
            if (menuSettingSelection.getSelectedFood() == null) {
                UserMethods.showError("Error", "Please select food");
                return;
            }
            menuSettingDetailPanel.add(
                new DetailPanel(
                        new Detail(),
                        new SettingDetailJTextField(menuSettingDetailPanel.getWidth(), menuSettingDetailPanel.getHeight()),
                        new SettingAddButton(menuSettingDetailPanel.getWidth(), menuSettingDetailPanel.getHeight())
                )
            );
            menuSettingDetailPanel.revalidate();
        });
        addDependencyButton.addActionListener(e -> {
            if (menuSettingSelection.getSelectedFood() == null) {
                UserMethods.showError("Error", "Please select food");
                return;
            }
            menuSettingDependencyPanel.add(
                    new DependencyPanel(
             null,
                        new SettingDetailJTextField(menuSettingDependencyPanel.getWidth(), menuSettingDependencyPanel.getHeight()),
                        new SettingAddButton(menuSettingDependencyPanel.getWidth(), menuSettingDependencyPanel.getHeight())
                    )
            );
            menuSettingDependencyPanel.revalidate();
        });
        saveSettingButton.addActionListener(e -> { //
            switch (settingTab.getSelectedIndex()) {
                case 0 -> {
                    //generalSetting
                    String password1 = new String(passwordField1.getPassword());
                    String password2 = new String(passwordField2.getPassword());
                    if (password1.isBlank() || password1.isEmpty() || password2.isBlank() || password2.isEmpty()) {
                        UserMethods.showError("Error", "Please input password");
                        return;
                    }
                    if (!password1.equals(password2)) {
                        UserMethods.showError("Error", "Password is not match");
                        return;
                    }
                    if (Inserter.insertPassword(password1)) {
                        UserMethods.showInfo("Success", "Successfully saved");
                    } else {
                        UserMethods.showError("Error", "Failed to save");
                    }
                }
                case 1 -> {
                    Food food;
                    try {
                        //menuSetting
                        food = menuSettingSelection.getSelectedFood();
                        food.setName(menuSettingName.getText());
                        food.setPrice(Integer.parseInt(menuSettingPrice.getText()));
                        food.setType(menuSettingGenre.getSelectedGenre());

                        food.clearDetails();
                        for (Component component : menuSettingDetailPanel.getComponents()) {
                            if (component.isVisible()) {
                                DetailPanel detailPanel = (DetailPanel) component;
                                Detail detail = detailPanel.getDetail();
                                detail.setName(detailPanel.getDetailName());

                                detail.clearOptions();
                                for (OptionPanel optionPanel : detailPanel.getOptionPanels()) {
                                    detail.addOption(optionPanel.getOptionName(), optionPanel.getOptionPrice());
                                }
                                food.addDetail(detail);
                            }
                        }

                        food.clearDependencies();
                        for (Component component : menuSettingDependencyPanel.getComponents()) {
                            if (component.isVisible()) {
                                DependencyPanel dependencyPanel = (DependencyPanel) component;
                                Dependency dependency = dependencyPanel.getDependency();

                                switch (dependencyPanel.getDependencyType()) {
                                    case FoodGenreType -> {
                                        dependency.setDependencyType(
                                            DependencyTypeClass.DependencyType.FoodGenreType);
                                        dependency.set(dependencyPanel.getGenre());
                                    }
                                    case FoodId -> {
                                        dependency.setDependencyType(
                                            DependencyTypeClass.DependencyType.FoodId);
                                        dependency.set(dependencyPanel.getFoodId());
                                    }
                                }

                                food.addDependency(dependency);
                            }
                        }
                    } catch (Exception exception) {
                        UserMethods.showError("Error", "Please input correct value");
                        return;
                    }

                    if (Inserter.insertFood(food)) {
                        UserMethods.showInfo("Success", "Successfully saved");
                    } else {
                        UserMethods.showError("Error", "Failed to save");
                    }

                }
                default -> throw new IllegalStateException("Unexpected value: " + settingTab.getSelectedIndex());
            }
        });
    }


    //Method

    /**.
     * 初期化処理
     * メインページに移動
     * メニューの初期化
     * メニューの表示
     * オーダーリストの初期化
     * 支払いページの初期化
     */
    private void init() {
        //totalPrice
        totalPrice.setText("0yen");
        menuSet = Selector.getMenu();
        MenuPanel menus = cg.createMenus(menuSet);
        //Tonkotu
        tonkotuRoot.removeAll();
        for (JButton panel : menus.getButton(FoodGenreType.Tonkotu)) {
            tonkotuRoot.add(panel);
        }
        //Shoyu
        shoyuRoot.removeAll();
        for (JButton panel : menus.getButton(FoodGenreType.Shoyu)) {
            shoyuRoot.add(panel);
        }
        //Saltflavored
        saltflanvoredRoot.removeAll();
        for (JButton panel : menus.getButton(FoodGenreType.Saltflavoured)) {
            saltflanvoredRoot.add(panel);
        }
        //Rice
        riceRoot.removeAll();
        for (JButton panel : menus.getButton(FoodGenreType.Rice)) {
            riceRoot.add(panel);
        }
        //Sidemenu
        sidemenuRoot.removeAll();
        for (JButton panel : menus.getButton(FoodGenreType.Side)) {
            sidemenuRoot.add(panel);
        }
        //Topping
        toppingRoot.removeAll();
        for (JButton panel : menus.getButton(FoodGenreType.Topping)) {
            toppingRoot.add(panel);
        }
        //Set
        setRoot.removeAll();
        for (JButton panel : menus.getButton(FoodGenreType.Set)) {
            setRoot.add(panel);
        }
        //Dessert
        dessertRoot.removeAll();
        for (JButton panel : menus.getButton(FoodGenreType.Dessert)) {
            dessertRoot.add(panel);
        }
        //Drink
        drinkRoot.removeAll();
        for (JButton panel : menus.getButton(FoodGenreType.Drink)) {
            drinkRoot.add(panel);
        }
        //OrderList
        orderListPlane.setLayout(new BoxLayout(orderListPlane, BoxLayout.Y_AXIS));
        orderFoodList.clear();
        orderListPlane.removeAll();
        //payment
        paymentTable.removeAll();
        //loadSetting

    }

    //MovePage
    private void goHome() {
        //MoveHomePage
        cardLayout.show(cardRoot, "home");
    }

    private void goMain() {
        //MoveMainPage
        cardLayout.show(cardRoot, "main");
    }

    private void goMain(FoodGenreType menuType) {
        goMain();
        switch (menuType) {
            case Tonkotu ->
                menu.setSelectedComponent(tonkotuRoot);
            case Shoyu ->
                menu.setSelectedComponent(shoyuRoot);
            case Saltflavoured ->
                menu.setSelectedComponent(saltflanvoredRoot);
            case Rice ->
                menu.setSelectedComponent(riceRoot);
            case Side ->
                menu.setSelectedComponent(sidemenuRoot);
            case Topping ->
                menu.setSelectedComponent(toppingRoot);
            case Set ->
                menu.setSelectedComponent(setRoot);
            case Dessert ->
                menu.setSelectedComponent(dessertRoot);
            case Drink ->
                menu.setSelectedComponent(drinkRoot);
            default -> throw new IllegalStateException("Unexpected value: " + menuType);
        }
    }

    private void goPayment() {
        //MovePaymentPage
        cardLayout.show(cardRoot, "payment");
        //テーブルに注文を追加
        OrderTableModel oftm = new OrderTableModel(this);
        paymentTable.setModel(oftm);
        paymentResultLabel.setText("Total: " + orderFoodList.getTotalPrice() + "yen");
    }

    private void goSetting() {
        PassfordForm pf = new PassfordForm(frame);
        String inputPassword = pf.getPassword();
        String password = Selector.getPassword();
        if (password.equals(inputPassword)) {
            cardLayout.show(cardRoot, "setting");
        } else {
            UserMethods.showError("Error", "Password is incorrect");
        }
    }

    private void goEnd() {
        //MoveEndPage
        cardLayout.show(cardRoot, "EndCard");
        endTimeDisplay.setText("Please waiting for 3 seconds");
        count = new AtomicInteger(0);
        displayTimer.setRepeats(true);
        endTimer.setRepeats(false);
        displayTimer.restart();
        endTimer.restart();
    }

    private void setUI() {
        //UI Setting
        HomeMain homeMain = new HomeMain(frame.getWidth(), frame.getHeight());
        goTonkotu.setUI(homeMain.set(MaterialColors.RED_400,
                menuSet.tonkotu.getFoods().getFirst().getImagePath()));
        goShoyu.setUI(homeMain.set(MaterialColors.ORANGE_400,
                menuSet.shoyu.getFoods().getFirst().getImagePath()));
        goSaltflavored.setUI(homeMain.set(MaterialColors.YELLOW_400,
                menuSet.saltflanvored.getFoods().getFirst().getImagePath()));

        HomeSub homeSub = new HomeSub(frame.getWidth(), frame.getHeight());
        goRice.setUI(homeSub.set(
                menuSet.rice.getFoods().getFirst().getImagePath()
        ));
        goSidemenu.setUI(homeSub.set(
                menuSet.side.getFoods().getFirst().getImagePath()
        ));
        goTopping.setUI(homeSub.set(
                menuSet.topping.getFoods().getFirst().getImagePath()
        ));
        goSet.setUI(homeSub.set(
                menuSet.set.getFoods().getFirst().getImagePath()
        ));
        goDessert.setUI(homeSub.set(
                menuSet.dessert.getFoods().getFirst().getImagePath()
        ));
        goDrink.setUI(homeSub.set(
                menuSet.drink.getFoods().getFirst().getImagePath()
        ));

        Submit submit = new Submit(frame.getWidth(), frame.getHeight());
        orderButton.setUI(submit);
        backToHomeButton.setUI(submit);
        confirmPaymentButton.setUI(submit);
        cancelPaymentButton.setUI(submit);
        exitButton.setUI(submit);
        saveSettingButton.setUI(submit);
        cancelSettingButton.setUI(submit);
        retunPaymentButton.setUI(submit);

        NonCustomerButton nonCustomerButton = new NonCustomerButton(frame.getWidth(), frame.getHeight());
        goSetting.setUI(nonCustomerButton);

        //Settting
        SettingDetailJTextField settingDetailJTextField = new SettingDetailJTextField(menuSettingPanel.getWidth(), menuSettingPanel.getHeight());
        menuSettingName.setUI(settingDetailJTextField.setMain());
        menuSettingPrice.setUI(settingDetailJTextField.setSub());

        //menu
        MainUI mainUI = new MainUI(frame.getWidth(), frame.getHeight());
        menu.setUI(mainUI.setSize(70, 100));

        //payment
        paymentTable.setRowHeight(200);
        paymentTable.setRowSelectionAllowed(false);
        paymentTable.setColumnSelectionAllowed(false);
        paymentPage.setFont(GeneralFont.mainFont(paymentTable));
    }

}
