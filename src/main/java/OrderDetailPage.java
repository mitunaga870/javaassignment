import component.DetailRadioButton;
import component.FontCustomedLabel;
import foods.Detail;
import foods.Food;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Map;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import order.OrderFood;
import order.SelectedDetail;
import order.SelectedDetailList;

/**.
 * 注文の詳細を弄るページ
 */
public class OrderDetailPage {
    //Variables
    private JDialog dialog;
    private JPanel root;
    private JPanel detailRoot;
    private JButton comfirmButton;
    private JButton cancelButton;
    private JSpinner amountPninner;
    private OrderFood result;
    private Food paramFood;
    private ArrayList<ButtonGroup> buttonGroups;


    /**.
     * コンストラクタ
     * @param frame 親となるフレーム
     * @param food 元となる食品
     */
    public OrderDetailPage(JFrame frame, Food food) {
        this.paramFood = food;
        dialog = new JDialog(frame, "OrderDetail", true);
        buttonGroups = new ArrayList<ButtonGroup>();

        detailRoot.setLayout(new BoxLayout(detailRoot, BoxLayout.Y_AXIS));
        //データ読み込み
        for (Detail detail : food.getDetails().get()) {
            FontCustomedLabel label = new FontCustomedLabel(detail.getName());
            label.setAlignmentX(FontCustomedLabel.LEFT_ALIGNMENT);
            JPanel optionPanel = new JPanel();
            optionPanel.setLayout(new FlowLayout());

            //ボタングループ
            ButtonGroup buttonGroup = new ButtonGroup();
            buttonGroups.add(buttonGroup);
            //デフォルトボタン
            JRadioButton defaultButton = new DetailRadioButton("none", new SelectedDetail(detail.getName()));
            defaultButton.setSelected(true);
            buttonGroup.add(defaultButton);
            optionPanel.add(defaultButton);
            //各オプション
            for (Map.Entry<String, Integer> option : detail.getOptions().entrySet()) {
                JRadioButton button = new DetailRadioButton(option.getKey() + " " + option.getValue() + "yen", new SelectedDetail(detail.getName(), option.getKey(), option.getValue()));
                buttonGroup.add(button);
                optionPanel.add(button);
            }
            detailRoot.add(label);
            detailRoot.add(optionPanel);
        }
        amountPninner.setValue(1);

        //ActionListener
        cancelButton.addActionListener(e -> {
            dialog.dispose();
        });
        comfirmButton.addActionListener(e -> {
            //エラーチェック
            int amount = (int) amountPninner.getValue();
            if (0 >= amount) {
                JOptionPane.showMessageDialog(dialog, "amount must be more than 0");
                return;
            }
            //帰り値の生成
            result = new OrderFood(paramFood);
            result.amount = amount;
            SelectedDetailList selectedDetailList = new SelectedDetailList();
            for (int i = 0; i < buttonGroups.size(); i++) {
                ButtonGroup buttonGroup = buttonGroups.get(i);
                for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
                    javax.swing.AbstractButton button = buttons.nextElement();
                    if (button.isSelected()) {
                        selectedDetailList.add(((DetailRadioButton) button).getValue());
                        break;
                    }
                }
            }
            result.setSelectedDetail(selectedDetailList);
            dialog.dispose();
        });
        cancelButton.addActionListener(e -> {
            dialog.dispose();
        });
    }

    /**
     * .
     * 注文の詳細を取得する
     *
     * @return food 注文の詳細を追加した食品
     */
    public OrderFood showDialog() {
        dialog.setContentPane(root);
        dialog.setSize(300, 300);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        return result;
    }
}
